// montando torres no DOM //

const boxGame = document.querySelector(".box-game")

const torreUm = document.createElement("div")
torreUm.classList.add("torre-1")
boxGame.appendChild(torreUm)

const torreDois = document.createElement("div")
torreDois.classList.add("torre-2")
boxGame.appendChild(torreDois)

const torreTres = document.createElement("div")
torreTres.classList.add("torre-3")
boxGame.appendChild(torreTres)

// montando discos no DOM //

const discoUm = document.createElement("div")
discoUm.classList.add("disco1")

const discoDois = document.createElement("div")
discoDois.classList.add("disco2")

const discoTres = document.createElement("div")
discoTres.classList.add("disco3")

const discoQuatro = document.createElement("div")
discoQuatro.classList.add("disco4")

// botando discos na torre inicial //

torreUm.appendChild(discoUm)
torreUm.appendChild(discoDois)
torreUm.appendChild(discoTres)
torreUm.appendChild(discoQuatro)


let modo = 'selecionar'
let arr = []

function changeTower(event) {
    if (modo === 'selecionar'
        &&
        event.currentTarget.childElementCount !== 0) { 
        modo = 'jogar' 
        arr.push(event.currentTarget.lastElementChild)
    } else if(modo === 'jogar') { 
            if (event.currentTarget.lastElementChild === null 
                ||
                event.currentTarget.lastElementChild.clientWidth > arr[arr.length - 1].clientWidth 
                ||
                arr.length === 0) { 
                if (event.currentTarget.childElementCount < 4) { 
                    event.currentTarget.appendChild(arr.pop()) // 
                    modo = 'selecionar' 
                }
            }
            else {
                modo = 'selecionar'
            }
    } 
    
    if (torreTres.childElementCount === 4) {
        alert("Vitória!")
    } 
}

torreUm.addEventListener('click', changeTower)

torreDois.addEventListener('click', changeTower)

torreTres.addEventListener('click', changeTower)